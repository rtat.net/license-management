#!/bin/bash -l

set -e

usage="
$(basename "$0") [-h]

$(basename "$0") analyze PROJECT_PATH

$(basename "$0") test

where:
  -h  show this help text
  PROJECT_PATH = the path of the project to be analyzed for licenses usage.

  Set SETUP_CMD to skip auto-detection and use the specified command to install project dependencies."

if [ $# -eq 0 ] ; then
  echo "$usage"
  exit 1
fi

while getopts 'h' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

COMMAND=$1
shift

# "/run.sh" as a command means the user want the "analyze" command.
if [ "$COMMAND" = "/run.sh" ] ; then
  COMMAND="analyze"
fi
if [ "$COMMAND" = "analyse" ] ; then
  COMMAND="analyze"
fi

# "/test/test.sh" as a command means the user want the "test" command.
if [ "$COMMAND" = "/test/test.sh" ] ; then
  COMMAND="test"
fi

# Check number of arguments

if [ "$COMMAND" = "analyze" -a $# -ne 1 ] ; then
  echo "$usage"
  exit 1
fi

if [ "$COMMAND" = "test" -a $# -ne 0 ] ; then
  echo "$usage"
  exit 1
fi

# Run command
case "$COMMAND" in
  test)
    # Run integration tests.
    exec /test/test.sh
    ;;

  analyze)
    # Analyze project

    # Load RVM
    source /rvm.sh

    # Change current directory to the project path.
    APP_PATH=$1
    shift
    pushd $APP_PATH

    if [[ -z "${SETUP_CMD}" ]]; then
        # Before running license_finder, we need to install dependencies for the project.
        if test -f Gemfile ; then
          if test -n "$rvm_recommended_ruby" ; then
            # Install the Ruby version RVM found in the project files
            # This always end in the cryptic "bash: Searching: command not found" error but Ruby is installed
            # So we ignore the error.
            $($rvm_recommended_ruby) 2>/dev/null || true
            rvm use .
            gem install bundler
            # We need to install the license_finder gem into this Ruby version too.
            gem install license_finder
          fi

          # Ignore test and development dependencies.
          license_finder ignored_groups add development
          license_finder ignored_groups add test
          bundle install --without "development test"
        fi

        if test -f requirements.txt ; then
          # Install Python Pip packages.
          pip install -r requirements.txt
        fi

        if test -f package.json ; then
          # Install NPM packages.
          npm install --production
          # Try to install Peer packages too, npm install doesn't do it anymore.
          /node_modules/.bin/npm-install-peers
        fi

        if test -f bower.json ; then
          # Install Bower packages.
          bower install
        fi

        # Symlink the project into GOPATH to allow fetching dependencies.
        ln -sf `realpath $APP_PATH` /gopath/src/app

        if test -f Godeps/Godeps.json ; then
          # Install Go dependencies with Godeps.
          pushd /gopath/src/app
          godep restore
          popd
        elif find . -name "*.go" -printf "found" -quit |grep found >/dev/null ; then
          # Install Go dependencies with go get.
          pushd /gopath/src/app
          go get
          popd
        fi

        if test -f pom.xml ; then
          # Install Java Maven dependencies.
          mvn install
        fi

        if test -f build.gradle ; then
            gradle build
        fi
    else
        echo "Running '${SETUP_CMD[@]}' to install project dependencies..."
        ${SETUP_CMD[@]}
    fi

    # Run License Finder.
    echo "Running license_finder $@ in $PWD"
    license_finder report --format=html --save=gl-license-management-report.html
    popd

    # Extract data from the HTML report and put it into a JSON file
    node /html2json.js $APP_PATH/gl-license-management-report.html > $APP_PATH/gl-license-management-report.json
    ;;

  *)
    # Unknown command
    echo "Unknown command: $COMMAND"
    echo "$usage"
    exit 1
esac
